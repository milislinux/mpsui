import gi
from gi.repository import Gtk

class IslemStack(Gtk.VBox):
	def __init__(self,ebeveyn):
		Gtk.VBox.__init__(self)
		self.ebeveyn = ebeveyn
		#Stack için bir ada ve bir başlığa ihtiyacımız var
		self.ad = "islem"
		self.baslik = self.ebeveyn.ceviri[20]


		spinner = Gtk.Spinner()
		self.pack_start(spinner,  expand = True, fill = True, padding = 5)
		spinner.start()

		self.yazi = Gtk.Label()
		self.pack_start(self.yazi, expand = False, fill = True, padding = 5)


	def yazi_ata(self,yazi):
		self.yazi.set_text(yazi)
