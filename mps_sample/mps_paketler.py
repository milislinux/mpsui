import gi, os
from gi.repository import Gtk, GdkPixbuf

class Paketler(Gtk.VBox):
	def __init__(self,ebeveyn):
		Gtk.VBox.__init__(self)
		#Paket için bir ada ve bir başlığa ihtiyacımız var
		self.ad = "paketler"
		self.ebeveyn = ebeveyn
		self.baslik = self.ebeveyn.ceviri[21]

		self.arama_kutu = Gtk.HBox()
		self.pack_start(self.arama_kutu,False,True,0)
		#Paket araması için bir entry oluşturuyoruz
		self.arama_kismi = Gtk.SearchEntry()
		self.arama_kismi.connect("search-changed",self.arama_degisti)
		self.arama_kutu.pack_start(self.arama_kismi, expand = True, fill = True, padding = 5)

		self.detayli_ara = Gtk.Button()
		self.detayli_ara.set_always_show_image(True)
		image = Gtk.Image(stock=Gtk.STOCK_FIND)
		self.detayli_ara.set_image(image)
		self.detayli_ara.set_label(self.ebeveyn.ceviri[22])
		self.detayli_ara.connect("clicked",self.detayli_ara_tiklandi)
		self.arama_kutu.pack_start(self.detayli_ara,False,False,0)

		self.dosyada_ara = Gtk.Button()
		self.dosyada_ara.set_always_show_image(True)
		image = Gtk.Image(stock=Gtk.STOCK_DIRECTORY)
		self.dosyada_ara.set_image(image)
		self.dosyada_ara.set_label(self.ebeveyn.ceviri[53])
		self.dosyada_ara.connect("clicked",self.dosyada_ara_tiklandi)
		self.arama_kutu.pack_start(self.dosyada_ara,False,False,0)

		#List store ve sinyallerini oluşturalım
		self.paketler_store = Gtk.ListStore(GdkPixbuf.Pixbuf(),str,str,str,str,bool)
		self.paketler_liste = Gtk.TreeView(model = self.paketler_store)
		self.paketler_liste.set_activate_on_single_click(True)
		self.paketler_liste.connect("button-release-event",self.sag_tik_menu)
		#self.paketler_liste.connect("row-activated",self.paketler_liste_tiklandi)
		#Sutunları oluşturup view a ekleyleim
		sutun_icon = Gtk.CellRendererPixbuf()
		sutun_icon.set_fixed_size(32,32)
		sutun = Gtk.TreeViewColumn(self.ebeveyn.ceviri[23],sutun_icon, gicon = 0)
		self.paketler_liste.append_column(sutun)
		sutun_text = Gtk.CellRendererText()
		sutun = Gtk.TreeViewColumn(self.ebeveyn.ceviri[24],sutun_text, text = 1)
		self.paketler_liste.append_column(sutun)
		sutun_text = Gtk.CellRendererText()
		sutun = Gtk.TreeViewColumn(self.ebeveyn.ceviri[25],sutun_text, text = 2)
		sutun.set_alignment(0.5)
		self.paketler_liste.append_column(sutun)
		sutun_text = Gtk.CellRendererText()
		sutun = Gtk.TreeViewColumn(self.ebeveyn.ceviri[26],sutun_text, text = 3)
		sutun.set_alignment(0.5)
		self.paketler_liste.append_column(sutun)
		sutun_text = Gtk.CellRendererText()
		sutun = Gtk.TreeViewColumn(self.ebeveyn.ceviri[41],sutun_text, text = 4)
		sutun.set_alignment(0.5)
		self.paketler_liste.append_column(sutun)
		sutun_toogle = Gtk.CellRendererToggle()
		#sutun_toogle.set_radio(True)
		sutun_toogle.connect("toggled",self.kur_sil_tiklandi)
		self.sutun_tur = Gtk.TreeViewColumn(self.ebeveyn.ceviri[27],sutun_toogle, active = 5)
		self.sutun_tur.set_clickable(True)
		self.sutun_tur.connect("clicked",self.coloumn_tiklandi)
		self.sutun_tur.set_alignment(0.5)
		self.paketler_liste.append_column(self.sutun_tur)

		#Scrool olması lazım scrolu oluşturup view'ı verelim
		scroll = Gtk.ScrolledWindow()
		scroll.set_policy(Gtk.PolicyType.AUTOMATIC,Gtk.PolicyType.AUTOMATIC)
		scroll.add(self.paketler_liste)
		#Scroola ekleyelim
		self.pack_start(scroll, True, True, 5)


	def coloumn_tiklandi(self,widget):
		menu = Gtk.Menu()

		m_t_isaretle = Gtk.MenuItem(self.ebeveyn.ceviri[29])
		m_t_isaretle.connect("activate",self.tumunu_isaretle)
		menu.add(m_t_isaretle)

		m_t_isaret_kaldir = Gtk.MenuItem(self.ebeveyn.ceviri[30])
		m_t_isaret_kaldir.connect("activate",self.isaretleri_kaldir)
		menu.add(m_t_isaret_kaldir)

		menu.show_all()
		menu.popup_at_pointer()

	def sag_tik_menu(self,widget,event):
		if event.button == 3:
			selection = self.paketler_liste.get_selection()
			#Modelle iteri alalım
			tree_model, tree_iter = selection.get_selected()
			#Şimdilik print ediyoruz ama dosyayı açacağız nasıl yapacaksak
			satir = tree_model[tree_iter][1]
			#satir = self.paketler_store[path]
			kurulu_mu = tree_model[tree_iter][4]
			menu = Gtk.Menu()
			m_bilgi = Gtk.MenuItem(self.ebeveyn.ceviri[28])
			m_bilgi.connect("activate",self.paketler_liste_tiklandi, satir)
			menu.add(m_bilgi)

			#Sadece kurulu paketler
			if kurulu_mu == self.ebeveyn.ceviri[43]:
				m_bilgi_kld = Gtk.MenuItem(self.ebeveyn.ceviri[45])
				m_bilgi_kld.connect("activate",self.paketler_liste_tiklandi, "-kdl", satir)
				menu.add(m_bilgi_kld)

			m_bilgi_gc = Gtk.MenuItem(self.ebeveyn.ceviri[46])
			m_bilgi_gc.connect("activate",self.paketler_liste_tiklandi, "-gc", satir)
			menu.add(m_bilgi_gc)

			m_bilgi_gd = Gtk.MenuItem(self.ebeveyn.ceviri[47])
			m_bilgi_gd.connect("activate",self.paketler_liste_tiklandi, "-gd", satir)
			menu.add(m_bilgi_gd)

			m_bilgi_gtc = Gtk.MenuItem(self.ebeveyn.ceviri[48])
			m_bilgi_gtc.connect("activate",self.paketler_liste_tiklandi, "-gtc", satir)
			menu.add(m_bilgi_gtc)

			m_bilgi_gdc = Gtk.MenuItem(self.ebeveyn.ceviri[49])
			m_bilgi_gdc.connect("activate",self.paketler_liste_tiklandi, "-gdc", satir)
			menu.add(m_bilgi_gdc)

			m_bilgi_pl = Gtk.MenuItem(self.ebeveyn.ceviri[50])
			m_bilgi_pl.connect("activate",self.paketler_liste_tiklandi, "-pl", satir)
			menu.add(m_bilgi_pl)

			m_bilgi_lg = Gtk.MenuItem(self.ebeveyn.ceviri[51])
			m_bilgi_lg.connect("activate",self.paketler_liste_tiklandi, "-lg", satir)
			menu.add(m_bilgi_lg)

			#Sadece kurulu paketler
			if kurulu_mu == self.ebeveyn.ceviri[43]:
				m_bilgi_pkd = Gtk.MenuItem(self.ebeveyn.ceviri[52])
				m_bilgi_pkd.connect("activate",self.paketler_liste_tiklandi, "-pkd", satir)
				menu.add(m_bilgi_pkd)

			menu.attach_to_widget(widget)
			menu.show_all()
			menu.popup_at_pointer()


	def isaretleri_kaldir(self,widget):
		for paket in self.ebeveyn.gorunen_paketler:
			if self.ebeveyn.islem == "Güncellenebilir Paketler" and paket in self.ebeveyn.guncellenecek_paketler:
				self.ebeveyn.guncellenecek_paketler.remove(paket)
			if self.ebeveyn.islem == "Derlenebilir Paketler" and paket in self.ebeveyn.derlenecek_paketler:
				self.ebeveyn.derlenecek_paketler.remove(paket)
			if self.ebeveyn.islem == "Silinebilir Paketler" or self.ebeveyn.islem == "Tüm Paketler":
				p = self.ebeveyn.paketler[paket]
				if p["durum"] == 'Kurulu' and paket in self.ebeveyn.silinecek_paketler:
					self.ebeveyn.silinecek_paketler.remove(paket)
			if self.ebeveyn.islem == "Kurulabilir Paketler" or self.ebeveyn.islem == "Tüm Paketler":
				p = self.ebeveyn.paketler[paket]
				if p["durum"] == 'Kurulu Değil' and paket in self.ebeveyn.kurulacak_paketler:
					self.ebeveyn.kurulacak_paketler.remove(paket)
		self.paketler_liste_doldur(self.ebeveyn.gorunen_paketler)
		self.ebeveyn.uygula_dugme.set_sensitive(True)


	def tumunu_isaretle(self,widget):
		for paket in self.ebeveyn.gorunen_paketler:
			if self.ebeveyn.islem == "Güncellenebilir Paketler" and paket not in self.ebeveyn.guncellenecek_paketler:
				self.ebeveyn.guncellenecek_paketler.append(paket)
			elif self.ebeveyn.islem == "Derlenebilir Paketler" and paket not in self.ebeveyn.derlenecek_paketler:
				self.ebeveyn.derlenecek_paketler.append(paket)
			elif self.ebeveyn.islem == "Silinebilir Paketler" or self.ebeveyn.islem == "Tüm Paketler":
				p = self.ebeveyn.paketler[paket]
				if p["durum"] == 'Kurulu' and paket not in self.ebeveyn.silinecek_paketler:
					self.ebeveyn.silinecek_paketler.append(paket)
			elif self.ebeveyn.islem == "Kurulabilir Paketler" or self.ebeveyn.islem == "Tüm Paketler":
				p = self.ebeveyn.paketler[paket]
				if p["durum"] == 'Kurulu Değil' and paket not in self.ebeveyn.kurulacak_paketler:
					self.ebeveyn.kurulacak_paketler.append(paket)
		self.paketler_liste_doldur(self.ebeveyn.gorunen_paketler)
		self.ebeveyn.uygula_dugme.set_sensitive(True)



	def detayli_ara_tiklandi(self, widget):
		arama = self.arama_kismi.get_text()
		arama = arama.lower()
		if arama == "":
			ust_yazi = self.ebeveyn.ceviri[31]
			bilgi = Gtk.MessageDialog(self.ebeveyn, 0, Gtk.MessageType.ERROR, Gtk.ButtonsType.OK, ust_yazi)
			bilgi.set_title(self.ebeveyn.ceviri[1])
			bilgi.format_secondary_text(self.ebeveyn.ceviri[32])
			bilgi.run()
			bilgi.destroy()
		else:
			komut = "mps ara -a {}".format(arama)
			self.ebeveyn.stack_degistir(0)
			self.ebeveyn.term.komutlar.append(komut)
			self.ebeveyn.term.komut_calistir()
			self.ebeveyn.stack_liste[0].yazi_ata(self.ebeveyn.ceviri[33])


	def dosyada_ara_tiklandi(self, widget):
		arama = self.arama_kismi.get_text()
		arama = arama.lower()
		if arama == "":
			ust_yazi = self.ebeveyn.ceviri[31]
			bilgi = Gtk.MessageDialog(self.ebeveyn, 0, Gtk.MessageType.ERROR, Gtk.ButtonsType.OK, ust_yazi)
			bilgi.set_title(self.ebeveyn.ceviri[1])
			bilgi.format_secondary_text(self.ebeveyn.ceviri[32])
			bilgi.run()
			bilgi.destroy()
		else:
			komut = "mps sor --hp {}".format(arama)
			self.ebeveyn.stack_degistir(0)
			self.ebeveyn.term.komutlar.append(komut)
			self.ebeveyn.term.komut_calistir()
			self.ebeveyn.stack_liste[0].yazi_ata(self.ebeveyn.ceviri[54])


	def kur_sil_tiklandi(self, widget, path):
		satir = self.paketler_store[path]
		if satir[5]:
			#Kurulacak yada silinecek paketler arasından paketi silinecek	
			satir[5] = False
			if satir[4] == self.ebeveyn.ceviri[42]:
				self.ebeveyn.kurulacak_paketler.remove(satir[1])
			elif satir[4] == self.ebeveyn.ceviri[43]:
				self.ebeveyn.silinecek_paketler.remove(satir[1])
			elif satir[4] == self.ebeveyn.ceviri[44]:
				self.ebeveyn.guncellenecek_paketler.remove(satir[1])
			elif satir[4] == self.ebeveyn.ceviri[56]:
				self.ebeveyn.derlenecek_paketler.remove(satir[1])
		else:
			#Kurulacak yada silinecek paketlere ekle
			satir[5] = True
			if satir[4] == self.ebeveyn.ceviri[42]:
				self.ebeveyn.kurulacak_paketler.append(satir[1])
			elif satir[4] == self.ebeveyn.ceviri[43]:
				self.ebeveyn.silinecek_paketler.append(satir[1])
			elif satir[4] == self.ebeveyn.ceviri[44]:
				self.ebeveyn.guncellenecek_paketler.append(satir[1])
			elif satir[4] == self.ebeveyn.ceviri[56]:
				self.ebeveyn.derlenecek_paketler.append(satir[1])
		if len(self.ebeveyn.silinecek_paketler) == 0 and len(self.ebeveyn.kurulacak_paketler) == 0 \
		and len(self.ebeveyn.guncellenecek_paketler) == 0 and len(self.ebeveyn.derlenecek_paketler) == 0:
			self.ebeveyn.uygula_dugme.set_sensitive(False)
		else:
			self.ebeveyn.uygula_dugme.set_sensitive(True)

	def arama_degisti(self,aranan):
		"""Aranan Değiştiğinde bu fonksiyon çalışacak"""
		#Widgettten yazıyı alalım
		aranan = aranan.get_text()
		aranan = aranan.lower()
		#Bulunan paketleri buraya dolduracağız
		paket_liste = []
		#Tüm paketleri alıp döngüye verelim
		if self.ebeveyn.islem == "Güncellenebilir Paketler":
			paketler = self.ebeveyn.gun_paketler.keys()
		elif self.ebeveyn.islem == "Derlenebilir Paketler":
			paketler = self.ebeveyn.derlenebilir_paketler.keys()
		else:
			paketler = self.ebeveyn.paketler.keys()
		for paket in paketler:
			#Aranan kelime bizim paket adının içine varsa
			if aranan in paket:
				#Paketleri ekleyelim
				paket_liste.append(paket)
		#Doldursun diye fonksiyona verelim
		self.paketler_liste_doldur(paket_liste)



	def paketler_liste_doldur(self, paketler=None):
		"""Paketleri listeye ekleyeceğimiz fonksiyon bu"""
		#Paketler None ise paketler sözlüğünden keysleri alıyoruz
		if paketler == None:
			if self.ebeveyn.islem == "Güncellenebilir Paketler":
				paketler = self.ebeveyn.gun_paketler.keys()
			elif self.ebeveyn.islem == "Derlenebilir Paketler":
				paketler = self.ebeveyn.derlenebilir_paketler.keys()
			else:
				paketler = self.ebeveyn.paketler.keys()
		#Storu temizleyelim
		self.paketler_store.clear()
		self.ebeveyn.gorunen_paketler = []
		#Varsayılan simge temasını alalım
		simge_tema = Gtk.IconTheme.get_default()
		#Paketleri döngüye verelim
		for paket in paketler:
			if self.ebeveyn.islem == "Güncellenebilir Paketler":
				if paket in self.ebeveyn.gun_paketler.keys():
					paket_ = self.ebeveyn.gun_paketler[paket]
					islem = self.ebeveyn.ceviri[44]
				else:
					continue
			elif self.ebeveyn.islem == "Derlenebilir Paketler":
				if paket in self.ebeveyn.derlenebilir_paketler.keys():
					paket_ = self.ebeveyn.derlenebilir_paketler[paket]
					islem = self.ebeveyn.ceviri[56]
				else:
					continue
			else:
				if paket in self.ebeveyn.paketler.keys():
					paket_ = self.ebeveyn.paketler[paket]
				else:
					continue
				#Eğer Paket Kur işlemi yapılacaksa Kurulu paketleri görmemeiz gerek yok
				if self.ebeveyn.islem == "Kurulabilir Paketler":
					if paket_["durum"] == "Kurulu":
						continue
					else:
						islem = self.ebeveyn.ceviri[42]
				#Eğer paket Silinecekse Kurulu olmayan paketleri görmeye gerek yok
				elif self.ebeveyn.islem == "Silinebilir Paketler":
					if paket_["durum"] == "Kurulu Değil":
						continue
					else:
						islem = self.ebeveyn.ceviri[43]
				elif self.ebeveyn.islem == "Tüm Paketler":
					if paket_["durum"] == "Kurulu Değil":
						islem = self.ebeveyn.ceviri[42]
					elif paket_["durum"] == "Kurulu":
						islem = self.ebeveyn.ceviri[43]
					else:
						continue

			#Şartlarda sıkıntı yoksa paketi ekleyeceğiz
			try:
				#Temada icon varmı almayı deniyoruz
				icon = simge_tema.load_icon(paket,32,Gtk.IconLookupFlags.FORCE_SIZE)
			except:
				#Yoksa varolan simgemizi ekliyoruz
				if os.path.exists("/usr/milis/mpsui/mps_icons/paket.png"):
					icon = GdkPixbuf.Pixbuf.new_from_file("/usr/milis/mpsui/mps_icons/paket.png")
				else:
					icon = GdkPixbuf.Pixbuf.new_from_file("./mps_icons/paket.png")
			#İşlem tama Stora ekleyelim
			if paket in self.ebeveyn.kurulacak_paketler or paket in self.ebeveyn.silinecek_paketler or \
				(paket in self.ebeveyn.guncellenecek_paketler and self.ebeveyn.islem == "Güncellenebilir Paketler") or \
				(paket in self.ebeveyn.derlenecek_paketler and self.ebeveyn.islem == "Derlenebilir Paketler"):
				self.paketler_store.append([icon,paket,paket_["sürüm"],paket_["devir"],islem,True])
				self.ebeveyn.gorunen_paketler.append(paket)
			else:
				self.paketler_store.append([icon,paket,paket_["sürüm"],paket_["devir"],islem,False])
				self.ebeveyn.gorunen_paketler.append(paket)
		#Seçili olarak 0.yı yapalım
		self.paketler_liste.set_cursor(0)


	def paketler_liste_tiklandi(self,widget,paket,parametre=""):
		"""Paket listeye çift tıklanınca olacak işleri yapacağız"""
		self.ebeveyn.term.komutlar.append("mps bil {} {}".format(paket,parametre))
		self.ebeveyn.term.komut_calistir()
		self.ebeveyn.alt_tasiyici.show()
