import gi, os
from gi.repository import Gtk, GLib

class Add_Edit_Repo(Gtk.Window):
	def __init__(self,ebeveyn,repos,ceviri,is_add=False):
		Gtk.Window.__init__(self)
		box = Gtk.VBox()
		self.add(box)
		self.ceviri = ceviri
		self.repos = repos
		self.ebeveyn = ebeveyn
		self.is_add = is_add
		self.set_default_size(480,140)

		h_box_1 = Gtk.HBox()
		box.pack_start(h_box_1,0,0,5)
		bolum_label = Gtk.Label()
		bolum_label.set_text(self.ceviri[59])
		h_box_1.pack_start(bolum_label,0,0,5)

		self.bolum_combo = Gtk.ComboBoxText()
		h_box_1.pack_start(self.bolum_combo,1,1,5)
		r = []
		for rep in repos:
			if rep[0] not in r:
				r.append(rep[0])
				self.bolum_combo.append_text(rep[0])
				if is_add and is_add[0] == rep[0]:
					self.bolum_combo.set_active(r.index(rep[0]))

		h_box_2 = Gtk.HBox()
		box.pack_start(h_box_2,0,0,5)
		anahtar_label = Gtk.Label()
		anahtar_label.set_text(self.ceviri[60])
		h_box_2.pack_start(anahtar_label,0,0,5)
		self.anahtar_entry = Gtk.Entry()
		h_box_2.pack_start(self.anahtar_entry,1,1,5)

		h_box_3 = Gtk.HBox()
		box.pack_start(h_box_3,0,0,5)

		deger_label = Gtk.Label()
		deger_label.set_text(self.ceviri[61])
		h_box_3.pack_start(deger_label,0,0,5)
		self.deger_entry = Gtk.Entry()
		h_box_3.pack_start(self.deger_entry,1,1,5)

		add_remove_btn = Gtk.Button()
		if not is_add:
			self.set_title(self.ceviri[63])
			add_remove_btn.set_label(self.ceviri[63])
			add_remove_btn.connect("clicked",self.add_repo)
		else:
			self.anahtar_entry.set_text(is_add[1])
			self.deger_entry.set_text(is_add[2])

			edit_btn = Gtk.Button()
			edit_btn.set_label(self.ceviri[68])
			edit_btn.connect("clicked",self.edit_repo)
			box.pack_start(edit_btn,1,1,5)

			self.set_title(self.ceviri[67])
			add_remove_btn.set_label(self.ceviri[64])
			add_remove_btn.connect("clicked",self.remove_repo)
		box.pack_start(add_remove_btn,1,1,5)

	def edit_repo(self,widget):
		b = self.bolum_combo.get_active_text()
		a = self.anahtar_entry.get_text()
		d = self.deger_entry.get_text()
		if b != self.is_add[0] or a != self.is_add[1] or d != self.is_add[2]:
			self.repos.remove(list(self.is_add))
			self.repos.append([b,a,d])
			self.write_repos()
			self.ebeveyn.get_settings()
			self.destroy()
		else:
			bilgi = Gtk.MessageDialog(self, 0, Gtk.MessageType.ERROR, Gtk.ButtonsType.OK, self.ceviri[35])
			bilgi.set_title(self.ceviri[35])
			bilgi.format_secondary_text(self.ceviri[70])
			bilgi.run()
			bilgi.destroy()

	def add_repo(self,widget):
		b = self.bolum_combo.get_active_text()
		a = self.anahtar_entry.get_text()
		d = self.deger_entry.get_text()
		if b != None and a != "" and d != "":
			add = True
			for repo in self.repos:
				if repo[0] == b and repo[1] == a:
					bilgi = Gtk.MessageDialog(self, 0, Gtk.MessageType.ERROR, Gtk.ButtonsType.OK, self.ceviri[35])
					bilgi.set_title(self.ceviri[35])
					bilgi.format_secondary_text(self.ceviri[66])
					bilgi.run()
					bilgi.destroy()
					add = False
			if add:
				settings = GLib.KeyFile()
				for repo in self.repos:
					settings.set_string(*repo)
				settings.set_string(b,a,d)
				settings.save_to_file("/etc/mps.ini")
				self.ebeveyn.get_settings()
				self.destroy()
		else:
			bilgi = Gtk.MessageDialog(self, 0, Gtk.MessageType.ERROR, Gtk.ButtonsType.OK, self.ceviri[35])
			bilgi.set_title(self.ceviri[35])
			bilgi.format_secondary_text(self.ceviri[65])
			bilgi.run()
			bilgi.destroy()

	def remove_repo(self,widget):
		b = self.bolum_combo.get_active_text()
		a = self.anahtar_entry.get_text()
		d = self.deger_entry.get_text()
		soru = Gtk.MessageDialog(self,0,Gtk.MessageType.QUESTION, Gtk.ButtonsType.OK_CANCEL,self.ceviri[35])
		soru.set_title(self.ceviri[35])
		soru.format_secondary_text(self.ceviri[69].format(*self.is_add))
		cevap = soru.run()
		#Yapalım derse
		if cevap == Gtk.ResponseType.OK:
			self.repos.remove(list(self.is_add))
			self.write_repos()
		soru.destroy()
		self.ebeveyn.get_settings()
		self.destroy()

	def write_repos(self):
		settings = GLib.KeyFile()
		for repo in self.repos:
			settings.set_string(*repo)
		settings.save_to_file("/etc/mps.ini")

class Repos(Gtk.Window):
	def __init__(self,ebeveyn):
		Gtk.Window.__init__(self)
		self.ebeveyn = ebeveyn
		box = Gtk.VBox()
		self.add(box)
		
		self.repos_store = Gtk.ListStore(str,str,str)
		self.repos_liste = Gtk.TreeView(model = self.repos_store)
		self.repos_liste.set_activate_on_single_click(True)
		self.repos_liste.connect("button-release-event",self.repo_click)

		depo_turu = Gtk.CellRendererText()
		depo_turu_sutun = Gtk.TreeViewColumn(self.ebeveyn.ceviri[59],depo_turu, text = 0)
		self.repos_liste.append_column(depo_turu_sutun)

		depo_adi = Gtk.CellRendererText()
		depo_adi_sutun = Gtk.TreeViewColumn(self.ebeveyn.ceviri[60],depo_adi, text = 1)
		self.repos_liste.append_column(depo_adi_sutun)

		depo_adres = Gtk.CellRendererText()
		depo_adres_sutun = Gtk.TreeViewColumn(self.ebeveyn.ceviri[61],depo_adres, text = 2)
		self.repos_liste.append_column(depo_adres_sutun)

		scroll = Gtk.ScrolledWindow()
		scroll.set_policy(Gtk.PolicyType.AUTOMATIC,Gtk.PolicyType.AUTOMATIC)
		scroll.add(self.repos_liste)
		#Scroola ekleyelim
		box.pack_start(scroll, True, True, 5)
		self.set_default_size(640,480)
		self.set_title(self.ebeveyn.ceviri[62])

		add_button = Gtk.Button()
		add_button.connect("clicked",self.add_function)
		add_button.set_label(self.ebeveyn.ceviri[63])
		image = Gtk.Image()
		image.set_from_stock(Gtk.STOCK_ADD,Gtk.IconLookupFlags.FORCE_SIZE)
		add_button.set_image(image)
		box.pack_start(add_button, False, False, 5)
		self.get_settings()
		
		
		self.mkd = ["talimat","mkd","https://gitlab.com/milislinux/mkd23.git"]
		self.mkd_button = Gtk.Button()
		box.pack_start(self.mkd_button, False, False, 5)

		if self.mkd in self.repos:
			self.mkd_button.set_label(self.ebeveyn.ceviri[72])
			self.mkd_button.connect("clicked",self.mkd_function)
		else:
			self.mkd_button.set_label(self.ebeveyn.ceviri[71])
			self.mkd_button.connect("clicked",self.mkd_function)

	def mkd_function(self,widget):
		if self.mkd in self.repos:
			self.mkd_button.set_label(self.ebeveyn.ceviri[71])
			self.repos.remove(self.mkd)
		else:
			self.mkd_button.set_label(self.ebeveyn.ceviri[72])
			self.repos.append(self.mkd)
		settings = GLib.KeyFile()
		for repo in self.repos:
			settings.set_string(*repo)
		settings.save_to_file("/etc/mps.ini")
		self.get_settings()

	def repo_click(self,widget,event):
		if event.button == 1:
			selection = self.repos_liste.get_selection()
			#Modelle iteri alalım
			tree_model, tree_iter = selection.get_selected()
			#Şimdilik print ediyoruz ama dosyayı açacağız nasıl yapacaksak
			satir = tree_model[tree_iter]
			win = Add_Edit_Repo(self,self.repos,self.ebeveyn.ceviri,(satir[0],satir[1],satir[2]))
			win.show_all()

	def add_function(self,widget):
		win = Add_Edit_Repo(self,self.repos,self.ebeveyn.ceviri)
		win.show_all()

	def get_settings(self):
		self.repos = []
		self.repos_store.clear()
		settings = GLib.KeyFile()
		settings.load_from_file("/etc/mps.ini", GLib.KeyFileFlags.NONE)
		groups = settings.get_groups()
		for g in groups[0]:
			keys = settings.get_keys(g)
			for k in keys[0]:
				self.repos.append([g, k, settings.get_string(g,k)])
				self.repos_store.append([g, k, settings.get_string(g,k)])



