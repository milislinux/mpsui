import gi, os, pwd, datetime, json
from gi.repository import Vte, GLib, Gdk, Gio, Gtk
import subprocess

class Terminal(Vte.Terminal):
	def __init__(self,ebeveyn):
		Vte.Terminal.__init__(self)
		#Ebeveyndeki değişkenlere ulaşmak için
		self.ebeveyn = ebeveyn
		#Komutları ebeveyn buraya dolduracak bitene kadar dönecek
		self.komutlar = []
		#Spawn sync ile terminali oluşturuyoruz
		self.spawn_sync(Vte.PtyFlags.DEFAULT,
						os.path.expanduser('~'),
						["/bin/bash"],
						[],
						GLib.SpawnFlags.DO_NOT_REAP_CHILD,
						None,
						None)
		#Renkler için bir paket oluşturacağız
		palet = [
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			Gdk.RGBA(),
			]
		#Renklerimizi bir listeye veriyoruz
		renkler = [
			"#f8f8f2",
			"#272822",
			"#000000",
			"#f92672",
			"#a6e22e",
			"#e6db74",
			"#66d9ef",
			"#ae81ff",
			"#a1efe4",
			"#f8f8f2",
			"#000000",
			"#c61e5b",
			"#80af24",
			"#b3aa5a",
			"#50abbc",
			"#8b67cc",
			"#7fbcb3",
			"#cbcdc8",
			]
		#Paletlere renkleri ekleyelim
		for sayi in range(0,16):
			oldu_mu = palet[sayi].parse(renkler[sayi])
			if not oldu_mu:
				print("Renk okunamadı :: {}".format(renkler[sayi]))

		#Renkleri terminale ekleyelim
		self.set_colors(palet[0],palet[1],palet[2:])
		#Scroll hareket ettiğinde sinyal yayılacak bizde bittimi anlayacağız
		self.connect("child-exited", self.komut_bitti)

	def komut_bitti(self,term,pid):
		"""Komutun bitip bitmediğini anlamaya çalışacağız"""
		#Bittiyse dosya yazımı tamamlanmıştı dosyayı okuyalım
		okunan = self.takip_oku()
		#Çalışan komut komutların 0. arkadaşı
		calisan_komut = self.komutlar[0]
		#Her komut çalıştığında farklı sonuçlar olacak bu yüzden
		if calisan_komut == "mps sor -dpl --json":
			self.komut_parse_dpl(okunan)
		elif calisan_komut == "mps sor -kpl --json":
			self.komut_parse_kpl(okunan)
		elif calisan_komut == "mps sor -gtl 1 --json":
			self.komut_parse_gtl_1(okunan)
		elif calisan_komut == "mps gun -S --durum":
			self.komut_parse_gun(okunan)
		elif calisan_komut[:10] == "mps ara -a":
			self.komut_parse_arama(okunan)
		elif calisan_komut == "mps sor -gtl talimatname --json":
			self.komut_parse_derlenebilir(okunan)
		elif "mps der" in calisan_komut:
			os.system("xdg-open {} &".format(self.calisma_dizini))
			bilgi = Gtk.MessageDialog(self.ebeveyn, 0, Gtk.MessageType.INFO, Gtk.ButtonsType.OK, self.ebeveyn.ceviri[35])
			bilgi.set_title(self.ebeveyn.ceviri[35])
			bilgi.format_secondary_text(self.ebeveyn.ceviri[58])
			bilgi.run()
			bilgi.destroy()
		#Son çalışan komutları sileceğiz
		self.komutlar.pop(0)
		#Çalıştıracak komut bitmediyse tekrardan komutu siliyoruz
		if len(self.komutlar) != 0:
			self.komut_calistir()
		else:
			#Komut bitince paket ekranına atıyoruz
			if calisan_komut == "mps gun -S --durum":
				self.ebeveyn.paketler_dugme.remove(4)
				if len(self.ebeveyn.gun_paketler.keys()) == 0:
					self.ebeveyn.paketler_dugme.set_active(0)
					ust_yazi = self.ebeveyn.ceviri[34]
					bilgi = Gtk.MessageDialog(self.ebeveyn, 0, Gtk.MessageType.ERROR, Gtk.ButtonsType.OK, ust_yazi)
					bilgi.set_title(self.ebeveyn.ceviri[35])
					bilgi.format_secondary_text(self.ebeveyn.ceviri[36])
					bilgi.run()
					bilgi.destroy()
				else:
					self.ebeveyn.paketler_dugme.append_text(self.ebeveyn.ceviri[37])
					self.ebeveyn.paketler_dugme.set_active(4)
			if calisan_komut[:10] == "mps ara -a":
				self.ebeveyn.stack_degistir(1)
			elif calisan_komut[:7] != "mps bil":
				self.ebeveyn.stack_degistir(1)
				self.ebeveyn.stack_liste[1].arama_kismi.set_text("")
				self.ebeveyn.stack_liste[1].paketler_liste_doldur()
				print("Komutlar Bitti")

	def komut_parse_gtl_1(self,okunan):
		"""mps sur --tpl komutuyla var olan paketleri alıp listemize
		yazıyoruz ve kullanıcı silmek isterse uyarı veriyoruz."""
		gtls = json.loads(okunan)
		for gtl in gtls:
			self.ebeveyn.sistem_paketler.append(gtl["name"])

	def komut_parse_dpl(self,okunan):
		"""mps sur -dpl komutuyla var olan paketleri alıp listemize
		yazıyoruz tabi önce listeyi sıfırladıktan sonra"""
		self.ebeveyn.paketler = {}
		#Geleni satirlara ayiralim
		dpls = json.loads(okunan)
		for dpl in dpls:
			self.ebeveyn.paketler[dpl["name"]] = {"sürüm":dpl["version"],
											"devir":dpl["release"],
											"durum":"Kurulu Değil"}

	def komut_parse_kpl(self,okunan):
		"""mps sor -kpl komutu çalıştıktan sonra paket kurulu
		olarak işaretleyeceğiz"""
		#Geleni satirlara ayiralim
		kpls = json.loads(okunan)
		for kpl in kpls:
			sor = self.ebeveyn.paketler.get(kpl["name"],False)
			if sor:
				sor["durum"] = "Kurulu"
			else:
				self.ebeveyn.paketler[kpl["name"]] = {"sürüm":kpl["version"],
												"devir":kpl["release"],
												"durum":"Kurulu"}
		self.ebeveyn.paketler = dict(sorted(self.ebeveyn.paketler.items()))
		#PAKETLER HAZIR DEMEK PAKET PENCEREYE GEÇİŞ YAPALIM
		self.ebeveyn.stack_liste[1].paketler_liste_doldur()


	def komut_parse_arama(self,okunan):
		"""mps ara -a komutunu inceleyeceğiz"""
		bol = okunan.split("\n")
		bulunanlar = []
		for satir in bol:
			if satir != "":
				satir = satir.split()[0]
				satir = satir.split("#")[0]
				satir = satir.split("/")[-1]
				bulunanlar.append(satir)
		self.ebeveyn.stack_liste[1].paketler_liste_doldur(bulunanlar)

	def komut_parse_derlenebilir(self,okunan):
		"""mps sor -gtl talimatname --json komut çıktısından derlenebilir paketleri alacağız"""
		ders = json.loads(okunan)
		for der in ders:
			self.ebeveyn.derlenebilir_paketler[der["name"]] = {"sürüm":der["version"],"devir":der["release"],"durum":"Derlenecek"}
		self.ebeveyn.derlenebilir_paketler = dict(sorted(self.ebeveyn.derlenebilir_paketler.items()))

	def komut_parse_gun(self,okunan):
		"""mps gun -S --durum komutu çalıştıktan sonra paket kurulu
		olarak işaretleyeceğiz"""
		bol = okunan.split("\n")
		bol.sort()
		for satir in bol:
			if "->" not in satir: continue
			x = satir.split()
			pk  = x[0]
			old = "".join(satir.split("->")[0].split()[1:])
			new = satir.split("->")[1].split(":")[0]
			self.ebeveyn.gun_paketler[pk] = {"sürüm":old.split("-")[0]+" -> "+ new.split("-")[0],
											"devir":old.split("-")[1]+" -> "+ new.split("-")[1],
											"durum":"Guncellenecek"}
	def takip_oku(self):
		"""Takip ettiğimiz dosyayı okumaya çalışacağız"""
		try:
			f = open("/tmp/t_takip.txt")
			okunan = f.read()
			f.close()
		except:
			print("/tmp/t_takip.txt OKUNAMADI")
			okunan = ""
		return okunan


	def komut_calistir(self):
		"""Komut çalıştırılmak için bu fonksiyonu yazacağız"""
		#Terminali tekrardan güncelelyelim birşeyler yazan olur falan
		#Komutun çıktısını alabilmek için çıktıyı tmp altında t_takip.txt ye
		#Yazdıracağız o yüzden gelen komutu güncelliyoruz
		komut = "{} 2>&1 | tee /tmp/t_takip.txt".format(self.komutlar[0])
		komut = komut.replace("mps","/usr/milis/mps/bin/mps")
		f = open("/tmp/mps_ui_command.sh","w")
		f.write(komut)
		f.close()
		# /tmp/t_takip.txt dosyasının silinince durumun düzelidiği söylendiğinden
		# Dosyayı silelim
		if os.path.exists("/tmp/t_takip.txt"):
			os.remove("/tmp/t_takip.txt")
		# Feed Child versiyonu sıkıntı bu yüzden 2 farklı yöntem deniyoruz
		user_info = pwd.getpwuid(os.geteuid())
		self.calisma_dizini = user_info.pw_dir
		if "/usr/milis/mps/bin/mps der" in komut:
			aktif_zaman = datetime.datetime.now()
			self.calisma_dizini = os.path.join(self.calisma_dizini,".cache","mps","paketler",aktif_zaman.strftime("%Y-%m-%d-%H-%M-%S"))
			os.makedirs(self.calisma_dizini)
		a = self.spawn_sync(Vte.PtyFlags.DEFAULT,
						self.calisma_dizini,
						[user_info.pw_shell,"/tmp/mps_ui_command.sh"],
						[],
						GLib.SpawnFlags.DO_NOT_REAP_CHILD,
						None,
						None)
		self.watch_child(a[1])
