#!/usr/bin/python3

from setuptools import setup, find_packages
import os

datas = [("/usr/share/applications",["mps_data/mps_ui.desktop"]),
		("/usr/share/icons/hicolor/32x32/apps", ["mps_icons/paket.png"]),
		("/usr/share/icons/hicolor/scalable/apps",["mps_icons/paket.svg"]),
		("/usr/share/icons/hicolor/24x24/apps", ["mps_icons/paket_up.png"]),
		("/usr/share/icons/hicolor/scalable/apps",["mps_icons/paket_up.svg"])]

setup(
	name = "mps_ui",
	scripts = ["mps_ui"],
	packages = find_packages(),
	version = "0.9.1",
	description = "Milis Package Manager GTK Frontend",
	author = ["Fatih Kaya"],
	author_email = "sonakinci41@gmail.com",
	url = "https://mls.akdeniz.edu.tr/git/milislinux/mps-ui",
	data_files = datas
)
